package com.example.nvchien.controller;

import com.example.nvchien.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

//Class => search path
//TODO 1: Start flow from controller
@Controller
public class GreetingController {

    //Interface connect to Service
    @Autowired
    GreetingService greetingService;

    @GetMapping( path ="/hi")
    @ResponseBody
    public String hi() {
        return greetingService.hello();
    }

}

