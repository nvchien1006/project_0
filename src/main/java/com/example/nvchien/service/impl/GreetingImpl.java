package com.example.nvchien.service.impl;

import com.example.nvchien.mapper.Greetingmapper;
import com.example.nvchien.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//Class =>  write logic

//TODO 3
@Service
public class GreetingImpl implements GreetingService {

    //maper => interface connect to DB
    @Autowired
    Greetingmapper greetingmapper;

    @Override
    public String hello() {
        return "hello";
    }
}
